#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

int minRadius = 1;
int maxRadius = 100;

void trackBarChanged(int value, void* userData){

	minRadius = value+1;
	
}

void trackBar2Changed(int value, void* userData){

	maxRadius = value+1;
	
}

/*

	-c : apresenta o resultado sobre a matriz de borda de Canny
	

*/

int main(int argc, char** argv )
{
    bool canny = false;
	
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened()){  // check if we succeeded
		printf("not open file\n");
        return -1;
	}
	
	if(argc > 1){
	
		canny = std::string(argv[1]) == "-c"; // Works fine;
		
	}

    Mat edges;
    namedWindow("edges",1);
	//namedWindow("histogram",1);
	
	createTrackbar("min radius", "edges", NULL, 500, trackBarChanged, NULL);
	createTrackbar("max radius", "edges", NULL, 500, trackBar2Changed, NULL);
	
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
		
        GaussianBlur(frame, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
	
		Mat out = Mat(Size( edges.cols/2, edges.rows/2 ), edges.depth());
		Mat outColor = Mat(Size( edges.cols/2, edges.rows/2 ), edges.depth());
		pyrDown( edges, out );
		if(canny){
			pyrDown( edges, outColor );
		}else{
			pyrDown( frame, outColor );
		}
		std::vector<Vec3f> circles;
		Mat img;
		
		//void HoughCircles(InputArray image, OutputArray circles, int method, double dp, double minDist, double param1=100, double param2=100, int minRadius=0, int maxRadius=0 )¶
		HoughCircles(out, circles, HOUGH_GRADIENT,2 , out.rows/4, 200, 100, minRadius, maxRadius );
		
		for( size_t i = 0; i < circles.size(); i++ )
		{
			 Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
			 int radius = cvRound(circles[i][2]);
			 // draw the circle center
			 circle( outColor, center, 3, Scalar(0,255,0), -1, 8, 0 );
			 // draw the circle outline
			 circle( outColor, center, radius, Scalar(0,0,255), 3, 8, 0 );
		}
		
        imshow("edges", outColor);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}