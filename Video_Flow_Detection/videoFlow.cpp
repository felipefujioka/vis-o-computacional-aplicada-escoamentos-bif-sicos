#include <stdio.h>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
//using namespace std;

int minRadius = 1;
int maxRadius = 100;

void trackBarChanged(int value, void* userData){
	minRadius = value+1;
}

void trackBar2Changed(int value, void* userData){
	maxRadius = value+1;
}

/*
	-c : apresenta o resultado sobre a matriz de borda de Canny
*/

int main(int argc, char** argv )
{
    bool canny = false;
	bool camera = false;
	bool debug = false;
	char* file;
	
	for(int i = 0; i < argc; i++){
		if(std::string(argv[i])=="-canny"){
			canny = true;
		}else if(std::string(argv[i])=="-camera"){
			camera = true;
		}else if(std::string(argv[i])=="-d"){
			debug = true;
		}else if(i > 0){
			file = argv[i];
			printf("%s\n", file);
		}
		
	}
	
    VideoCapture cap;
	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480); 
	if(camera){
		cap = VideoCapture(0);	// open the default camera
	}else{
		cap = VideoCapture(file); // open a file
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	//usleep(200);
	
	cap.open(file);
	
	
    if(!cap.isOpened()){  // check if we succeeded
		printf("not open file\n");
        return -1;
	}else{
		printf("file opened successfully\n");
	}
	
	printf("number of frames = %f\n", cap.get(CAP_PROP_FRAME_COUNT));
	printf("format = %f\n", cap.get(CAP_PROP_FORMAT));	
    Mat edges;
    namedWindow("edges",1);
	
	createTrackbar("min radius", "edges", NULL, 500, trackBarChanged, NULL);
	createTrackbar("max radius", "edges", NULL, 500, trackBar2Changed, NULL);
	/*
	while(true){
		Mat output;
		// cap >> outpu t;
		if(cap.grab()){
			printf("image grabbed\n");
		}
		cap.retrieve(output);
		if(&output!=NULL){
			printf("image retrieved successfully!\n");
		}
		printf(" rows:%d columns:%d depth:%d\n", output.rows, output.cols, output.depth());
		cvtColor(output, output, COLOR_YCrCb2RGB);
		imshow("edges",output);
		if(waitKey(30) >= 0) break;
	}
	*/
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
		
        //cvtColor(frame, edges, COLOR_BGR2GRAY);
        GaussianBlur(frame, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
	
		Mat out = Mat(Size( edges.cols/2, edges.rows/2 ), edges.depth());
		Mat outColor = Mat(Size( edges.cols/2, edges.rows/2 ), edges.depth());
		pyrDown( edges, out );
		if(canny){
			pyrDown( edges, outColor );
		}else{
			pyrDown( frame, outColor );
		}
		
		std::vector<Vec3f> circles;
		Mat img;
		
		//void HoughCircles(InputArray image, OutputArray circles, int method, double dp, double minDist, double param1=100, double param2=100, int minRadius=0, int maxRadius=0 )¶
		HoughCircles(out, circles, HOUGH_GRADIENT,2 , out.rows/4, 200, 100, minRadius, maxRadius );
		
		for( size_t i = 0; i < circles.size(); i++ )
		{
			 Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
			 int radius = cvRound(circles[i][2]);
			 // draw the circle center
			 circle( outColor, center, 3, Scalar(0,255,0), -1, 8, 0 );
			 // draw the circle outline
			 circle( outColor, center, radius, Scalar(0,0,255), 3, 8, 0 );
		}
		
        imshow("edges", outColor);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}