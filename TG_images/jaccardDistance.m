function dist = jaccardDistance(A,B)

dist = 1 - sum(A & B)/sum(A | B);

end