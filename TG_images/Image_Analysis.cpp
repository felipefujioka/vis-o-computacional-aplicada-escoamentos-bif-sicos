
// Image_Analysis.cpp
//
// Created by Felipe Jun Fujioka Shida on 17th october 2014
// Copyright (c) 2014 All rights reserved
//

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat toBinary(std::string src){
	Mat image;		
	std::cout<<"file:"<<src<<"\n";

	image = imread(src,IMREAD_COLOR);
	Mat gray, equa, gauss, edges;

	cvtColor(image, gray, COLOR_RGB2GRAY);

	equalizeHist(gray, equa);

	GaussianBlur(equa, gauss, Size(11,11), 3, 3);

	Canny(gauss, edges, 0, 30, 3);
	
	return edges;
	
}

double jaccardDistance(Mat A, Mat B){

    int rows = A.rows;
    int cols = A.cols;
    
    double dist = 0;
    
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            dist += fabs((A.at<double>(i,j)-B.at<double>(i,j)))/(rows*cols);
        }
    }
    
    return dist;
    
}

double mahalaDistance(Mat A, Mat B){

	int rows = A.rows;
	int cols = A.cols;
	
	double error = 0;
	
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			float currError = A.at<float>(i,j)-B.at<float>(i,j);
			error += currError>0?currError:-1*currError;
			if(currError > 100.0 || currError < -100){
				std::cout<<"currError: "<<currError<<"\n";
				std::cout<<"A: "<<A.at<float>(i,j) <<" B: "<< B.at<float>(i,j)<<"\n";
			}
		}
	}
	
	return error/((double)rows*cols);
	
}

Mat normalizeMatrix(Mat A){

    int w = A.cols;
    int h = A.rows;
    
    double max = 0;
    
    for(int i = 0; i<w; i++){
        for(int j = 0; j<h;j++){
            double candidate = fabs(A.at<double>(i,j));
            if(candidate>max){
                max = candidate;
            }
        }
    }
    
    cout<<"max: "<<max<<"\n";
    
    return A/max;
    
}

double mahalDistance(Mat A, Mat B, Mat icovar){

    int l = A.rows;
    int a = A.cols;
    
    cout << "mahalanobis\n";
    
    A = A/(l*a);
    B = B/(l*a);
    
    Mat first = A-B;
    Mat second;
    
    transpose(first,second);
    
    cout << "first size:" << first.size() << " type: " << first.type()<<"\n";
    cout << "second size:" << second.size() << " type: " << second.type()<<"\n";
    cout << "icovar size:" << icovar.size() << " type: " << icovar.type()<<"\n";
    
    Mat dist;
    //cout<<"aqui antes\n";
    try {   
        dist = icovar*second*first;//*first;
    } catch (cv::Exception const & e) { 
        std::cerr<<"OpenCV exception: "<<e.what()<<std::endl; 
    }
    
    cout << "dist size:" << dist.size() << " type: " << dist.type()<<"\n";
    
   // cout <<"aqui\n";
    int w = dist.rows;
    int h = dist.cols;
    
    double m = 0;
    
    namedWindow("Dist", WINDOW_AUTOSIZE);
    imshow("Dist", dist);
    
    //m += fabs(dist.at<double>(i,j))/(w*h);
    
    for(int i = 0; i < w; i++){
        for(int j = 0; j < h; j++){
            //cout<<"i: "<<i<<"j: "<<j<<"__";
            m += fabs(dist.at<double>(i,j))/(w*h);
        }
    }
    

    
    cout<<"mahalanobis distance: "<<m<<"\n";
    
    return m;
    
}

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

int main( int argc, char** argv )
{
	
	bool canny = false;
	bool grayB = false;
	bool histogram = false;
	bool gaussian = false;
	bool hough = false;
	bool binarize = false;
    bool save = false;
    bool analyse = false;
	
	std::string binDirectory = "/0101";
	std::string test;
	
	int gaussianSize = 7;
	float deviation = 1.5;
	
	int minRadius = 10;
	int maxRadius = 50;
	if(argc > 1){
        for(int i = 1; i < argc; i++){

            std::string str = std::string(argv[i]);

            if(str=="-canny"){
                canny=true;
                gaussianSize = std::stoi(argv[i+1]);
                deviation = std::stof(argv[i+2]);

                std::cout << "gaussianSize=" << gaussianSize << " deviation=" << deviation<<"\n";
            }else if(str=="-gray"){
                std::cout<<"GRAY\n";
                grayB=true;
            }else if(str=="-histogram"){
                histogram=true;
            }else if(str=="-hough"){
                hough=true;
                minRadius = std::stoi(argv[i+1]);
                maxRadius = std::stoi(argv[i+2]);
                std::cout << "minRadius=" << minRadius << " maxRadius=" << maxRadius<<"\n";
            }else if(str=="-gaussian"){
                gaussian=true;
            }else if(str == "-binarize"){
                binarize = true;
                binDirectory = std::string(argv[i+1]);
                std::cout<<"bin directory: "<<binDirectory<<"\n";
                if(argv[i+2] != NULL){
                    test = std::string(argv[i+2]);
                }
            }else if(str == "-save"){
                save = true;
            }else if(str == "-analyse"){
                analyse = true;
            }else if(str == "-help"){
                std::cout<<"-canny FILTER_SIZE STANDARD_DEVIATION\n-gray: for grayscale\n-histogram: histogram equalization\n-hough MIN MAX: apply Hough filter with MIN and MAX radius\n-gaussian: show the gaussian smooth used for canny\n-binarize DIR_NAME: calculates the binary image for all samples in the directory";
                
            }
        }
	}else{
            std::cout<<"invalid sintax, use -help to find usages.\n";
    }

    Mat image;
  
	std::string files[] = {"/ImgA000000.tif","/ImgA000001.tif","/ImgA000002.tif","/ImgA000003.tif","/ImgA000004.tif","/ImgA000005.tif","/ImgA000006.tif","/ImgA000007.tif"};
	
	if(binarize){
	
		Mat samples[8];
		Mat meanImg;
        
        Mat image0102 = toBinary("0102/ImgA000000.tif");
        image0102.convertTo(image0102,5);
        
        Mat image0106 = toBinary("0106/ImgA000000.tif");
        image0106.convertTo(image0106,5);
        
        meanImg.create(1280,400,5);
        
		for(int i=0; i < 8; i++){
			samples[i] = toBinary(binDirectory+files[i]);
            samples[i].convertTo(samples[i],5);

		}
        
        meanImg.create(samples[1].rows, samples[1].cols, samples[1].type());
        
        for(int i = 0; i < 8; i++){
            cout<<"samples 0"<<i<<": "<<samples[i].cols<<" "<<samples[i].rows<< " "<< samples[i].type()<<"\n";
            cout<<"meanImg: "<<meanImg.cols<<" "<<meanImg.rows<< " "<< meanImg.type()<<"\n";
            meanImg+=samples[i];
        }
        
 
        
        meanImg = meanImg/8;
        
        namedWindow("Mean Image", WINDOW_AUTOSIZE);
		imshow("Mean Image", meanImg);
        
        //waitKey(0);
        
		Mat covar, mean;
        
        calcCovarMatrix(meanImg, covar, mean, COVAR_NORMAL | COVAR_ROWS, CV_8UC1);
        
        namedWindow("Covariance", WINDOW_NORMAL);
		imshow("Covariance", covar);
		namedWindow("Mean", WINDOW_AUTOSIZE);
		imshow("Mean", mean);
        
        Mat icovar;
        
        //invert(covar, icovar);
        
        icovar = covar.inv();
        if(save){
            Mat save;
            //cvtColor(icovar, save, COLOR_GRAY2RGB);
            imwrite( binDirectory+"/icovar.tif", save );
            Mat save2;
            //cvtColor(meanImg, save2, COLOR_GRAY2RGB);
            imwrite( binDirectory+"/mean.tif", save2 );
        }
        namedWindow("Inv Covar", WINDOW_AUTOSIZE);
		imshow("Inv Covar", icovar);

        Mat ma, mb;
        
        ma = samples[0];
        mb = meanImg;
        
        std::cout<<"type a:"<<ma.type()<<"\n";
        std::cout<<"type b:"<<mb.type()<<"\n";
        std::cout<<"mean type:"<<meanImg.type()<<"\n";
        std::cout<<"icovar type:"<<icovar.type()<<"\n";
        std::cout<<"a cols:"<<ma.cols<<"\n";
        std::cout<<"a rows:"<<ma.rows<<"\n";
        std::cout<<"b cols:"<<mb.cols<<"\n";
        std::cout<<"b rows:"<<mb.rows<<"\n";
        std::cout<<"mean cols:"<<meanImg.cols<<"\n";
        std::cout<<"mean rows:"<<meanImg.rows<<"\n";
        std::cout<<"icovar cols:"<<icovar.cols<<"\n";
        std::cout<<"icovar rows:"<<icovar.rows<<"\n";
        
        std::cout<<"a size:"<<ma.size()<<"\n";
        std::cout<<"b size:"<<mb.size()<<"\n";

        double dist = mahalDistance(ma,mb,icovar);
        
        std::cout << "mahalanobis 0101 -> 0101: " << endl;
        std::cout << dist << endl;
        
        dist = mahalDistance(image0102,mb,icovar);
        std::cout << "mahalanobis 0102 -> 0101: " << endl;
        std::cout << dist << endl;
        
        dist = mahalDistance(image0106,mb,icovar);
        std::cout << "mahalanobis 0106 -> 0101: " << endl;
        std::cout << dist << endl;
        
        waitKey(0);
        
		Mat scaled;
		calcCovarMatrix(samples,8, covar, mean, COVAR_SCRAMBLED | COVAR_SCALE | COVAR_ROWS);
		
		resize(covar, scaled, Size(covar.rows*50, covar.cols*50));

			
		waitKey(0);
	}
    
    if(analyse){
    
        std::string dirs[] = {"0101","0102","0106"};
        
        Mat i0101 = imread("0101/icovar.tif");
        Mat i0102 = imread("0102/icovar.tif");
        Mat i0106 = imread("0106/icovar.tif");
        
        Mat m0101 = imread("0101/mean.tif");
        Mat m0102 = imread("0102/mean.tif");
        Mat m0106 = imread("0106/mean.tif");
        
        Mat ms[] = {m0101,m0102,m0106};
        Mat is[] = {i0101,i0102,i0106};
        
        for(int i = 0; i < 3; i++){
            cvtColor(ms[i],ms[i],COLOR_RGB2GRAY);
            cvtColor(is[i],is[i],COLOR_RGB2GRAY);
            ms[i].convertTo(ms[i],5);
            is[i].convertTo(is[i],5);
        }
        
        while(true){
            
            int d = rand() % 3;
            int f = rand() % 8;
            
            std::string path = dirs[d]+files[f];
            
            Mat loaded = imread(path);
            
            namedWindow("Loaded", WINDOW_AUTOSIZE);
            imshow("Loaded", loaded);
            
            Mat bin = toBinary(path);
            
            bin.convertTo(bin,5);
            //cvtColor(bin,bin,COLOR_GRAY2RGB);
            
            int max = 0;
            double dist = INFINITY;
            
            for(int i = 0; i < 3; i++){
                
                Mat mb = ms[i];
                
                
                
                std::cout<<"type a:"<<bin.type()<<"\n";
                std::cout<<"type b:"<<mb.type()<<"\n";
                std::cout<<"icovar type:"<<is[i].type()<<"\n";
                std::cout<<"a cols:"<<bin.cols<<"\n";
                std::cout<<"a rows:"<<bin.rows<<"\n";
                std::cout<<"b cols:"<<mb.cols<<"\n";
                std::cout<<"b rows:"<<mb.rows<<"\n";
                std::cout<<"icovar cols:"<<is[i].cols<<"\n";
                std::cout<<"icovar rows:"<<is[i].rows<<"\n";
                
                
                double local = mahalDistance(bin,mb,is[i]);
                double jaccard = jaccardDistance(bin,mb);
                cout<<"jaccardDist: "<<jaccard<<"\n";
                if(local<dist){
                    dist = local;
                    max = i;
                }
            }
            cout<<"BEST FIT: ";
            switch(max){
                case 0:
                    cout << "0101\n";
                break;
                case 1:
                    cout << "0102\n";
                break;
                case 2:
                    cout << "0106\n";
                break;
            }
            cout << path << "\n";
            
            waitKey(0);
        }
    }
	
	if(canny){
		
		image = imread(argv[1], IMREAD_COLOR);   // Read the file

		if(! image.data )                              // Check for invalid input
		{
			cout <<  "Could not open or find the image" << std::endl ;
			return -1;
		}

		namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
		imshow( "Display window", image );                   // Show our image inside it.
		
		namedWindow("Canny", WINDOW_AUTOSIZE);
		Mat edges, gray, equa;
		cvtColor(image, gray, COLOR_RGB2GRAY);
		
		if(grayB){
			namedWindow("Gray", WINDOW_AUTOSIZE);
			imshow("Gray", gray);
		}
		
		if(histogram){
			namedWindow("Equalized", WINDOW_AUTOSIZE);
			equalizeHist(gray, gray);
			imshow("Equalized", gray);
		}

		if(gaussian){
			GaussianBlur(gray, gray, Size(gaussianSize,gaussianSize), deviation, deviation);
			namedWindow("Gaussian smooth", WINDOW_AUTOSIZE);
			imshow("Gaussian smooth", gray);
		}
		
		if(equa.empty()){
			printf("equalized matrix empty\n");
		}else{
			printf("equalized matrix set\n");
		}
		
        Canny(gray, gray, 0, 30, 3);
		imshow("Canny", gray);
		
		if(hough){
			std::vector<Vec3f> circles;
			Mat img;
			Mat out = image.clone();
			HoughCircles(edges, circles, HOUGH_GRADIENT,2 , edges.rows/4, 200, 100, minRadius, maxRadius );

			for( size_t i = 0; i < circles.size(); i++ )
			{
				 Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
				 int radius = cvRound(circles[i][2]);
				 // draw the circle center
				 circle( out, center, 3, Scalar(0,255,0), -1, 8, 0 );
				 // draw the circle outline
				 circle( out, center, radius, Scalar(0,0,255), 3, 8, 0 );
			}
			namedWindow("Hough", WINDOW_AUTOSIZE);
			imshow("Hough", out);
		}
		
		waitKey(0);                                          // Wait for a keystroke in the window
	}
	
    return 0;
}