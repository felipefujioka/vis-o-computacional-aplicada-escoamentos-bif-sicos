function A = binarize(image)

im = image(:,:,1:3);

gray = rgb2gray(im);

%figure, imshow(gray);

equalized = histeq(gray);

%figure, imshow(equalized);

%figure, imshow(im);

gaussianFilter = fspecial('gaussian', 11, 3);

smooth = imfilter(equalized, gaussianFilter);

%figure, imshow(smooth);

binary = edge(smooth, 'canny');

%figure, imshow(binary);

A = binary;

end


