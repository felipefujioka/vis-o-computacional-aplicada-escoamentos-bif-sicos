function mean = meanOfDirName(dirName)

images = zeros(400,1280,8);

for i=[0:7]

    name = strcat(dirName,'/ImgA00000');
    name = strcat(name, num2str(i));
    name = strcat(name,'.tif');
    subject = imread(name);
    subject = imresize(subject,[400 1280]);
    binary = binarize(subject);
    images(:,:,i+1) = binary;
end

defSize = size(subject);
meanImg = zeros(defSize(1), defSize(2));

for i = [1:8]

    meanImg = meanImg+images(:,:,i);

end

meanImg = meanImg/8;
    
mean = meanImg;

end