clc, clear all, close all;

i0101 = imread('0101/meanImg.tif');
i0102 = imread('0102/meanImg.tif');
i0104 = imread('0104/meanImg.tif');
i0106 = imread('0106/meanImg.tif');

i0201 = imread('0201/meanImg.tif');
i0202 = imread('0202/meanImg.tif');
i0204 = imread('0204/meanImg.tif');
i0206 = imread('0206/meanImg.tif');

i0401 = imread('0401/meanImg.tif');
i0402 = imread('0402/meanImg.tif');
i0403 = imread('0404/meanImg.tif');
i0406 = imread('0406/meanImg.tif');

i0601 = imread('0601/meanImg.tif');
i0602 = imread('0602/meanImg.tif');
i0604 = imread('0604/meanImg.tif');
i0606 = imread('0606/meanImg.tif');

i0801 = imread('0801/meanImg.tif');
i0802 = imread('0802/meanImg.tif');
i0804 = imread('0804/meanImg.tif');
i0806 = imread('0806/meanImg.tif');

candidates = zeros(400,1280,16);

candidates(:,:,1) = i0101;
candidates(:,:,2) = i0102;
candidates(:,:,3) = i0104;
candidates(:,:,4) = i0106;

candidates(:,:,5) = i0201;
candidates(:,:,6) = i0202;
candidates(:,:,7) = i0204;
candidates(:,:,8) = i0206;

candidates(:,:,9) = i0401;
candidates(:,:,10) = i0402;
candidates(:,:,11) = i0404;
candidates(:,:,12) = i0406;

candidates(:,:,13) = i0601;
candidates(:,:,14) = i0602;
candidates(:,:,15) = i0604;
candidates(:,:,16) = i0606;

candidates(:,:,17) = i0801;
candidates(:,:,18) = i0802;
candidates(:,:,19) = i0804;
candidates(:,:,20) = i0806;


while 1
    
    f = randi([0 19]);
    i = randi([8 9]);
    
    if f == 0
        filename = '0101/ImgA00000';
    elseif f == 1
        filename = '0102/ImgA00000';
    elseif f == 2
        filename = '0104/ImgA00000';
    elseif f == 3
        filename = '0106/ImgA00000';
    elseif f == 4
        filename = '0201/ImgA00000';
    elseif f == 5
        filename = '0202/ImgA00000';
    elseif f == 6
        filename = '0204/ImgA00000';
    elseif f == 7
        filename = '0206/ImgA00000';
    elseif f == 8
        filename = '0401/ImgA00000';
    elseif f == 9
        filename = '0402/ImgA00000';
    elseif f == 10
        filename = '0404/ImgA00000';
    elseif f == 11
        filename = '0406/ImgA00000';
    elseif f == 12
        filename = '0601/ImgA00000';
    elseif f == 13
        filename = '0602/ImgA00000';
    elseif f == 14
        filename = '0604/ImgA00000';
    elseif f == 15
        filename = '0606/ImgA00000';
    elseif f == 16
        filename = '0801/ImgA00000';
    elseif f == 17
        filename = '0802/ImgA00000';
    elseif f == 18
        filename = '0804/ImgA00000';
    elseif f == 19
        filename = '0806/ImgA00000';
    end
   
    filename = strcat(filename, num2str(i));
    filename = strcat(filename, '.tif')
    
    image = imread(filename);
    
    bin = binarize(image);
    
    min = 1;
    cand = 0;
    
    for i = [1:20]
        d = jaccardDistance(bin, candidates(:,:,i));
        if d < min
            min = d;
            cand = i;
        end
    end
    
    'BEST: '
    if f == 0
        '0101'
    elseif f == 1
        '0102'
    elseif f == 2
        '0104'
    elseif f == 3
        '0106'
    elseif f == 4
        '0201'
    elseif f == 5
        '0202'
    elseif f == 6
        '0204'
    elseif f == 7
        '0206'
    elseif f == 8
        '0401'
    elseif f == 9
        '0402'
    elseif f == 10
        '0404'
    elseif f == 11
        '0406'
    elseif f == 12
        '0601'
    elseif f == 13
        '0602'
    elseif f == 14
        '0604'
    elseif f == 15
        '0606'
    elseif f == 16
        '0801'
    elseif f == 17
        '0802'
    elseif f == 18
        '0804'
    elseif f == 19
        '0806'
    end
    
    waitforbuttonpress
end

